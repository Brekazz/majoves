<!DOCTYPE html>
<html lang="es">
<head>
  <?php include_once("template/head.php"); ?>
  <style>
    .navbar{
      background:#fff;
    }

    #navbar > ul > li > a{
      color: #1ab394;
    }
    
    .footer{
      background: #fff;
      border-top: 1px solid;
    }
  </style>
</head>
<body id="page-top">

<!--NAVBAR-->
<?php include_once("template/navbar.php"); ?>

<!--CONTACTOS-->
<section class="gray-section contact pt-8">
  <div class="container">

    <div class="row">
      <div class="col-lg-12 text-center">
        <h1><b><span class="navy"><i>Medios de contacto</i></span></b></h1>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-lg-offset-3">
          <address>
            <strong><span class="navy">Majoves S.A.S.</span></strong><br>
            <i class="fa fa-map-marker"></i> Cra 15B No. 27D-22 <br>
            Barranquilla, Colombia<br>
            <i class="fa fa-envelope"></i> info@majoves.com.co<br>
            <i class="fa fa-phone"></i> (+57) 311-2399066 | 323-3649237
          </address>
        </div>
      </div>
    </div>

    <!--QUEJAS-->
    <div class="col-xs-6 wow zoomIn animated">

      <div class="row">
        <div class="col-lg-12 text-center">
          <h3><span class="navy">Quejas y Reclamos</span></h3>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8 col-xs-offset-2 text-center">
          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Nombres" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Apellidos" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Email" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Empresa" class="form-control">
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Cargo" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Telefono/Celular" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <textarea placeholder="Mensaje" class="form-control" cols="10" rows="5"></textarea>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <a href="mailto:test@email.com" class="btn btn-primary">Enviar</a>
            </div>
          </div>

        </div>
      </div>

    </div>

    <!--CONTACTENOS-->
    <div class="col-xs-6 wow zoomIn animated">

      <div class="row">
        <div class="col-lg-12 text-center">
          <h3><span class="navy">Contactenos</span></h3>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8 col-xs-offset-2 text-center">
          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Nombres" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Email" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <input placeholder="Asunto" class="form-control">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <textarea placeholder="Mensaje" class="form-control" cols="10" rows="5"></textarea>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-10">
              <a href="mailto:test@email.com" class="btn btn-primary">Enviar</a>
            </div>
          </div>

        </div>
      </div>

    </div>

  </div>

  <br>
</section>

<!--MAPA-->
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12" style="padding: 0;">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3916.596984638877!2d-74.80555105978327!3d10.993761787550977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8ef42d734ce53191%3A0x36096a35aad42f5e!2sParque+Suri+Salcedo!5e0!3m2!1ses-419!2sco!4v1543440406004" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
</div>

<!--FOOTER-->
<?php include_once("template/footer.php"); ?>

<!--SCRIPTS-->
<?php include_once("template/scripts.php"); ?>

</body>
</html>