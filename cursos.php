<!DOCTYPE html>
<html lang="es">
<head>
<?php include_once("template/head.php"); ?>
</head>
<body id="page-top">

<!--NAVBAR-->
<?php include_once("template/navbar.php"); ?>

<!--CURSOS-->
<section class="pt-8 cursos">
  <div class="container">
    <hr>
    <div class="row pb-3">
      <div class="col-lg-12 text-center">
        <h1>
          <b>
            CAPACITATE EN LOS CURSOS QUE OFRECE MAJOVES S.A.S
          </b>
        </h1>
        <p class="otext-white">
          Este tipo de formación tiene como objetivo complementar 
          o actualizar el conocimiento de las personas en áreas específicas. 
          La certificación se obtiene al momento de finalizar y aprobar el curso.
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
          
        <div class="row">
          <div class="col-sm-6 col-md-3 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/ManejoDefensivoPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4><span class="navy">Manejo Defensivo</span></h4>
                <p>
                  Prevención al conducir en vias.
                  <hr>
                  <span class="navy">Duración:</span> 40 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInRight">
            <div class="thumbnail">
              <img alt="" src="img/SustanciasPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4><span class="navy">Sustancias Peligrosas</span></h4>
                <p>
                    Prevención en el cuidado de estas sustancias.
                  <hr>
                  <span class="navy">Duración:</span> 60 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/PrimerosAuxiliosPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Primeros Auxilios Básicos
                  </span>
                </h4>
                <p>
                  Desempeñarse en prestacion de auxilios.
                  <hr>
                  <span class="navy">Duración:</span> 10 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInRight">
            <div class="thumbnail">
              <img alt="" src="img/ControlIncendiosPEQ.png" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Control de Incendios
                  </span>
                </h4>
                <p>
                  Mantener el control sobre la situaciones de Incendios.
                  <hr>
                  <span class="navy">Duración:</span> 25 Horas
                </p>  
              </div> 
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6 col-md-3 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/ExtintoresPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Manejo de Extintores
                  </span>
                </h4>
                <p>
                  Saber utilizar los elementos para controlar cualquira emergencia.
                  <hr>
                  <span class="navy">Duración:</span> 25 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInRight">
            <div class="thumbnail">
              <img alt="" src="img/CursoAlturasPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Altura Nivel Avanzado
                  </span>
                </h4>
                <p>
                    Tener los conocimientos para subir alturas.
                  <hr>
                  <span class="navy">Duración:</span> 40 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInLeft">
              <div class="thumbnail">
                <img alt="" src="img/AislamientoPEQ.png" height="100%" width="100%">
                <div class="caption">
                  <h4>
                    <span class="navy">
                      Alistamiento de Vehiculos
                    </span>
                  </h4>
                  <p>
                    Alistar un vehiculo para trasladarlo a el lugar indicado.
                    <hr>
                    <span class="navy">Duración:</span> 40 Horas
                  </p>  
                </div> 
              </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInRight">
            <div class="thumbnail">
              <img alt="" src="img/MecanicaPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Mecanica Básica
                  </span>
                </h4>
                <p>
                  Desempeñarse en manenimiento básico en mecanica.
                  <hr>
                  <span class="navy">Duración:</span> 40 Horas
                </p>  
              </div> 
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6 col-md-3 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/SeguridadPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Seguridad Vial
                  </span>
                </h4>
                <p>
                  Saber dirigir y prevenir cualquier accidente.
                  <hr>
                  <span class="navy">Duración:</span> 40 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInRight">
            <div class="thumbnail">
              <img alt="" src="img/NormasPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Normas de Tránsito
                  </span>
                </h4>
                <p>
                  Saber transitar por las vias y mantener el cuidado con las señalizaciónes.
                  <hr>
                  <span class="navy">Duración:</span> 40 Horas
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-3 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/MaquinariaPEQ.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Operadores de Gruas,
                    Montacargas, Retroexcavadora
                  </span>
                </h4>
                <p>
                  Conducir maquinarias pesadas.
                  <hr>
                  <span class="navy">Duración:</span> 120 Horas
                </p>  
              </div> 
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</section>

<!--FOOTER-->
<?php include_once("template/footer.php"); ?>

<!--SCRIPTS-->
<?php include_once("template/scripts.php"); ?>

</body>
</html>
