<!DOCTYPE html>
<html lang="es">
<head>
  <?php include_once("template/head.php"); ?>
</head>
<body id="page-top">

<!--NAVBAR-->
<?php include_once("template/navbar.php"); ?>

<!--CAROUSEL-->
<div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#inSlider" data-slide-to="0" class="active"></li>
    <li data-target="#inSlider" data-slide-to="1"></li>
    <li data-target="#inSlider" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="container">
        <div class="carousel-caption blank">
          <h1>
            CAPACITATE <br>
            EN LOS CURSOS <br>
            QUE OFRECE MAJOVES S.A.S
          </h1>
          <p>Capacitaciones y Certificaciones.</p>
          <p><a class="btn btn-lg btn-primary" href="#" role="button">Ver Mas</a></p>
        </div>
        <div class="carousel-image wow zoomIn">
          <img width="500" height="350" src="img/test.jpg" alt="Primeros Auxilios"/>
        </div>
      </div>
      <!-- Set background for slide in css -->
      <div class="header-back one"></div>
    </div>
    <div class="item">
      <div class="container">
        <div class="carousel-caption blank">
            <span class="navy">
              <h1>
                ENTERATE DE <br>
                ESPECIALIDADES Y CATEGORÍAS <br>
                EN LAS QUE PUEDES <br>
                CERTIFICARTE
              </h1>
              <p>complementa o actualiza tus conocimientos.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Ver mas</a></p>
            </span>
        </div>
      </div>
      <!-- Set background for slide in css -->
      <div class="header-back two"></div>
    </div>
    <div class="item">
      <div class="container">
        <div class="carousel-caption blank">
          <h1 class="text-border">
            CONOCE LOS SERVICIOS <br>
            QUE MAJOVES TIENE PARA TI
          </h1>
          <p><a class="btn btn-lg btn-primary" href="#" role="button">Ver mas</a></p>
        </div>
      </div>
      <!-- Set background for slide in css -->
      <div class="header-back three"></div>
    </div>
  </div>
  <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
  </a>
</div>

<!--PARTNERS-->
<section id="team" class="gray-section team">
  <div class="container">
    <div class="row m-b-lg">
      <div class="col-lg-12 text-center">
        <div class="navy-line"></div>
        <h1>Empresas Aliadas</h1>
      </div>
    </div>
    <div class="row">
      <div class="row pb-3">
        <div class="col-sm-3 wow fadeInLeft">
          <div class="team-member">
            <img src="img/Suliquido.png" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Suliquido</span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member wow zoomIn">
            <img src="img/puerto.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Puerto de Barranquilla</span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member wow zoomIn">
            <img src="img/SuraTrans.png" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Suratrans</span></h4>
          </div>
        </div>
        <div class="col-sm-3 wow fadeInRight">
          <div class="team-member">
            <img src="img/TransOil.png" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Transoil</span></h4>
          </div>
        </div>
      </div>

      <div class="row pb-3">
        <div class="col-sm-3 wow fadeInLeft">
          <div class="team-member">
            <img src="img/hernandez.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Transportes Hernandez</span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member wow zoomIn">
            <img src="img/Petromil.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Petromil</span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member wow zoomIn">
            <img src="img/MegaComercial.png" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Mega Comercial</span></h4>
          </div>
        </div>
        <div class="col-sm-3 wow fadeInRight">
          <div class="team-member">
            <img src="img/ecopetrol.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Ecopetrol</span></h4>
          </div>
        </div>
      </div>

      <div class="row pb-3">
        <div class="col-sm-3 wow fadeInLeft">
          <div class="team-member">
            <img src="img/tie.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Tie</span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member wow zoomIn">
            <img src="img/Avanade.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Avanade</span></h4>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="team-member wow zoomIn">
            <img src="img/BravoTrans.jpg" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Bravotrans</span></h4>
          </div>
        </div>
        <div class="col-sm-3 wow fadeInRight">
          <div class="team-member">
            <img src="img/integral.png" class="img-responsive img-circle img-small" alt="">
            <h4><span class="navy">Integral</span></h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--GALERIA-->
<div class="container">

  <div class="row m-b-lg">
    <div class="col-lg-12 text-center">
      <div class="navy-line"></div>
      <h1>Galeria</h1>
      <p>Aquí podemos presenciar los accidentes automovilísticos, y tomar prevención ante ellos.</p>
    </div>
  </div>

  <div class="col-sm-8 m-b-lg">
    <div class="embed-responsive embed-responsive-16by9">
      <video id="videoPrincipal" class="embed-responsive-item" controls>
        <source id="videoSource" src="videos/video1.mp4" type="video/mp4">
      </video>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="pre-scrollable">
    
    <div class="col-md-12 col-xs-12">
      <a class="thumbnail itemVideo" href="videos/video1.mp4">
        <img alt="100%x180" src="img/imgVideo1.png">
        <p class="text-center">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
      </a>
    </div>
    
    <div class="col-md-12 col-xs-12">
      <a class="thumbnail itemVideo" href="videos/video2.mp4">
        <img alt="100%x180" src="img/imgVideo2.png">
        <p class="text-center">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
      </a>
    </div>

    <div class="col-md-12 col-xs-12">
      <a class="thumbnail itemVideo" href="videos/video3.mp4">
        <img alt="100%x180" src="img/imgVideo3.png">
        <p class="text-center">Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
      </a>
    </div>

    </div>
  </div>

</div>

<a href="https://api.whatsapp.com/send?phone=573017330591&text=Hola!%20Quiero%20saber%20mas%20acerca%20de%20MAJOVES!" class="btn-whatsapp btn btn-primary">
  <i class="fa fa-whatsapp" aria-hidden="true"></i>
  Whatsapp
</a>

<!--FOOTER-->
<?php include_once("template/footer.php"); ?>

<!--SCRIPTS-->
<?php include_once("template/scripts.php"); ?>

<script>
$(document).ready(function(){
  $('a.itemVideo').on('click', function(e){
    e.preventDefault();

    $("#videoSource").attr("src", this.href);
    $("#videoPrincipal").load();
  });

});
</script>
</body>
</html>