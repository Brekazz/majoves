<!DOCTYPE html>
<html lang="es">
<head>
  <?php include_once("template/head.php"); ?>
</head>
<body id="page-top">

<!--NAVBAR-->
<?php include_once("template/navbar.php"); ?>

<!--MISION VISION-->
<section class="container-fluid pt-8 misionvision">
  <hr>
  <div class="row">
    <div class="col-sm-6 text-center">
      <h2>VISION</h2>
      <p class="text-justify misionvision">
        Ser líderes en la prestación de servicios educativos, 
        capacitaciones y asesorías en el transporte de hidrocarburos 
        en todo el territorio nacional, apoyados en los lineamientos 
        de la gestión educativa moderna del estado. Para el año 2018, 
        establecimos un convenio de capacitación con la empresa de 
        prestancia como lo es Mesintercol S.A.S, COOTRASENA y COSTRANSVIAL 
        S.A.S que en su visión establece que para el 2021 estará 
        posicionado en el mercado nacional como la empresa de vanguardia 
        en capacitación de personal operativo, en traslados de hidrocarburos, 
        mantenimiento, obra y reparación, en todo el ramo que las necesidades 
        para que el transporte de estos productos sean necesarios.
      </p>
    </div>
    <div class="col-sm-6 text-center">
      <h2>MISION</h2>
      <p class="text-justify misionvision">
        Contribuir con el mejoramiento socio-cultural de los 
        sectores Agroindustrial, de Comercio y Servicios, 
        mediante la formación, educación, capacitación y 
        asesorías, con personal idóneo y de alta calificación 
        técnica y pedagógica. Ya que aspiramos a crear una cultura 
        de seguridad en todas las personas que de una manera u 
        otra tienen que ver con el manejo de productos altamente 
        contaminantes, y así evitar situaciones inseguras que 
        llevan a accidentes y/o incidentes y bajar la tasa de 
        accidentalidad. Proporcionar servicios para la gestión 
        del ciclo de vida de los activos fijos de nuestros clientes 
        relacionados con el mantenimiento, reparación de las 
        maquinarias para la perforación de los pozos petroleros, 
        de gas natural, transporte de carga seca e hidrocarburos 
        así como la capacitación de los operadores de mercancías 
        peligrosas de forma tal que incremente su competitividad, 
        competencia y eficacia a corto y mediano plazo.
      </p>
    </div>
  </div>
</section>

<!--POLITICAS DE CALIDAD-->
<section class="container features">
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="navy-line"></div>
      <h1>POLITICA DE CALIDAD</h1>
    </div>
  </div>
  <div class="row features-block">
    <div class="col-lg-6 features-text wow fadeInLeft animated text-justify" style="visibility: visible; animation-name: fadeInLeft;">
      <small>Majoves</small>
      <p>
        Majoves S.A.S se compromete en la búsqueda de la 
        satisfacción de las necesidades de nuestros clientes 
        en lo que al transporte, mantenimiento preventivo y 
        correctivo, reparaciones, obras civiles, industriales 
        y de transporte.
      </p>
      <p>
        Para esto cuenta con un recurso humano idóneo, con 
        alta capacidad técnica, permitiendo así mantener el 
        mejoramiento continuo de los procesos, como el cumplimiento 
        de los requisitos legales y los establecidos por el cliente 
        prestando nuestros servicios de capacitación y certificación 
        para el trabajo, garantizando a nuestros clientes calidad, 
        seguridad, responsabilidad en salud ocupacional ya que 
        contamos con personal altamente calificado y certificado 
        por los entes de seguridad (SENA – Ministerio de Salud y 
        Protección Social).
      </p>
    </div>
    <div class="col-lg-6 text-right wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
      <img src="img/Calidad.png" height="50%" width="40%" alt="dashboard" class="img-responsive pull-right">
    </div>
  </div>
</section>

<!--REGISTROS-->
<section class="container features">
  <div class="row">
    <div class="col-lg-12 text-center">
      <div class="navy-line"></div>
      <h1>Registros Mensuales</h1>
    </div>
  </div>
  <div class="row features-block">
    <div class="col-lg-6 features-text wow fadeInLeft animated text-justify">
      <h3>Reportes:</h3>
    </div>
    <div class="col-sm-6 wow fadeInRight animated">
        <canvas id="ReporteVisitas"></canvas>
    </div>
    <div class="col-sm-6 wow fadeInRight animated">
        <canvas id="ReporteServicios"></canvas>
    </div>
  </div>
</section>

<!--FOOTER-->
<?php include_once("template/footer.php"); ?>

<!--SCRIPTS-->
<?php include_once("template/scripts.php"); ?>
<script src="js/chart.min.js"></script>

<script>
  function loadViews() {
    let ctx = document.getElementById('ReporteVisitas').getContext('2d');
    let chart = new Chart(ctx, {
      type: 'line',

      data: {
        labels: [
          "Enero", "Febrero", "Marzo", 
          "Abril", "Mayo", "Junio", 
          "Julio", "Agosto", "Septiembre", 
          "Octubre", "Noviembre", "Diciembre"
        ],
        datasets: [{
          label: "Visitas por mes",
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: [0, 10, 5, 2, 20, 30, 45],
        }]
      },
      options: {}
    });
  }

  function loadReports() {
    let ctx = document.getElementById('ReporteServicios').getContext('2d');
    let chart = new Chart(ctx, {
      type: 'line',

      data: {
        labels: [
          "Enero", "Febrero", "Marzo", 
          "Abril", "Mayo", "Junio", 
          "Julio", "Agosto", "Septiembre", 
          "Octubre", "Noviembre", "Diciembre"
        ],
        datasets: [{
          label: "Visitas por mes",
          backgroundColor: 'rgb(255, 99, 132)',
          borderColor: 'rgb(255, 99, 132)',
          data: [0, 1, 2, 3, 4, 3, 1],
        }]
      },
      options: {}
    });
  }
  
  loadViews();
  loadReports();
</script>

</body>
</html>
