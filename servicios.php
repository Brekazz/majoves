<!DOCTYPE html>
<html lang="es">
<head>
<?php include_once("template/head.php"); ?>
</head>
<body id="page-top">

<!--NAVBAR-->
<?php include_once("template/navbar.php"); ?>

<!--CURSOS-->
<section class="pt-8 cursos">
  <div class="container">
    <hr>

    <div class="row pb-3">
      <div class="col-lg-12 text-center">
        <h1>
          <b>
            Servicios Diponibles
          </b>
        </h1>
      </div>
    </div>

    <div class="row">
      <div class="col-md-8 col-md-offset-2">

        <div class="col-md-12">
          <div class="col-sm-6 col-md-6 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/1.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Pruebas hidrostáticas a tanques, semi – remolques y remolques.
                  </span>
                </h4>
                <p>
                  Mantenimiento en los equipos.
                  <hr>
                  <span class="navy">Duración:</span> 2 Meses
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-6 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/2.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                    Inspecciones en King- Ping, 5ta. Rueda en todos los modelos.
                  </span>
                </h4>
                <p>
                  Mantenimiento en los equipos.
                  <hr>
                  <span class="navy">Duración:</span> 2 Meses
                </p>  
              </div> 
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="col-sm-6 col-md-6 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/3.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                  Mantenimiento preventivo y correctivo de vehículos rígidos y tractocamiones.
                  </span>
                </h4>
                <p>
                  Mantenimiento en los equipos.
                  <hr>
                  <span class="navy">Duración:</span> 2 Meses
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-6 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/descarga.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                  Asesorías empresariales
                  </span>
                </h4>
                <p>
                  Mantenimiento en los equipos.
                  <hr>
                  <span class="navy">Duración:</span> 2 Meses
                </p>  
              </div> 
            </div>
          </div>
        </div>

        <div class="col-md-12">
          <div class="col-sm-6 col-md-6 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/7.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                  Mecánica de patio para equipos de obras civiles.
                  </span>
                </h4>
                <p>
                  Mantenimiento en los equipos.
                  <hr>
                  <span class="navy">Duración:</span> 2 Meses
                </p>  
              </div> 
            </div>
          </div>

          <div class="col-sm-6 col-md-6 wow fadeInLeft">
            <div class="thumbnail">
              <img alt="" src="img/0.jpg" height="100%" width="100%">
              <div class="caption">
                <h4>
                  <span class="navy">
                  Mecánica de patio para equipos de obras civiles.
                  </span>
                </h4>
                <p>
                  Mantenimiento en los equipos.
                  <hr>
                  <span class="navy">Duración:</span> 2 Meses
                </p>  
              </div> 
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
</section>

<!--FOOTER-->
<?php include_once("template/footer.php"); ?>

<!--SCRIPTS-->
<?php include_once("template/scripts.php"); ?>
</body>
</html>
