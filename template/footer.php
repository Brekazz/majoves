<section class="gray-section contact footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <ul class="list-inline social-icon pt-2">
          <li><a href="#"><i class="fa fa-twitter"></i></a>
          </li>
          <li><a href="#"><i class="fa fa-facebook"></i></a>
          </li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a>
          </li>
        </ul>
      </div>
    </div>
      <div class="row">
          <div class="col-lg-8 col-lg-offset-2 text-center">
              <p><strong>&copy; 2018 Majoves S.A.S</strong><br/>Todos los derechos reservados.</p>
          </div>
      </div>
  </div>
</section>