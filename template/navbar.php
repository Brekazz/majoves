<div class="navbar-wrapper">
  <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">
          <img src="img/Logo.jpg" width="80" height="50" alt="">
        </a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li id="index"><a class="page-scroll" href="index.php">Inicio</a></li>
          <li id="quienessomos"><a class="page-scroll" href="quienessomos.php">¿Quienes Somos?</a></li>
          <li id="cursos"><a class="page-scroll" href="cursos.php">Cursos</a></li>
          <li id="servicios"><a class="page-scroll" href="servicios.php">Servicios</a></li>
          <li id="contactenos"><a class="page-scroll" href="contactenos.php">Contacto</a></li>
          <li id="correo"><a class="page-scroll" href="correo.php">Correo</a></li>
          <li id="ingresar"><a class="page-scroll btn btn-warning" href="ingresar.php">Ingresar</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>