<script src="js/jquery-2.1.1.js"></script>
<script src="js/pace.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/inspinia.js"></script>
<?php
  $uri = explode("/", $_SERVER['PHP_SELF']);
  $uri = end($uri);
  $uri = strstr($uri, ".php", true);
?>
<script>
$(document).ready(function(){
  $('#<?=$uri?>').addClass("active");
});
</script>